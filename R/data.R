#' S4 ShortRead object for reference barcodes.
#'
#' fa: an ShortRead object with a length of 500 reads and a width of 18 cycles.
#' @title fasta file
#' @format an ShortRead object with a length of 500 reads and a width of 18 cycles:
#' \describe{
#'   \item{id}{barcode id}
#'   \item{sread}{barcodes}
#'   ...
#' }
#' @source \url{https://gitlab.oit.duke.edu/owzar001/bios824-notebook-spring2019/}
"fasta"

#' S4 ShortReadQ object for sequencing reads.
#'
#' rfq: an ShortReadQ object with a length of 10,000 reads and a width of 18 cycles.
#' @title fastq file
#' @format an ShortReadQ object with a length of 10,000 reads and a width of 18 cycles:
#' \describe{
#'   \item{id}{barcode id}
#'   \item{sread}{barcodes}
#'   \item{quality}{Phred scores for each base call}
#'   ...
#' }
#' @source \url{https://gitlab.oit.duke.edu/owzar001/bios824-notebook-spring2019/}
"fastq"
