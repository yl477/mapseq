#' this is a function created for determining the unmatched bases between read and ref
#'
#' @param seq1 string type input of sequence1
#' @param seq2 string type input of sequence2
#' @return numeric output of the index/indices of mismatched base(s)
#' @export

# determine the unmatched bases between read and ref
unmatch_index <- function(seq1, seq2) {
  seq1 <- as.character(seq1)
  indices <- c()
  for (i in 1:nchar(seq1)) {
    L1 <- substr(seq1, i, i)
    L2 <- substr(seq2, i, i)
    if (L1 != L2) {
      indices <- append(indices, i)
    }
  }
  return(indices)
}
