---
title: 'Mapseq: a Proof-of-Principle Package for Calculating Proportion of Mapped
  Sequencing Reads for a Given Reference Barcode Library'
author: "Yiwen Liu (yiwen.liu@duke.edu)"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Documentation}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---
---
title: 'Mapseq: a Proof-of-Principle Package for Calculating Proportion of Mapped
  Sequencing Reads for a Given Reference Barcode Library'
author: "Yiwen Liu (yiwen.liu@duke.edu)"
output:
  html_document:
    df_print: paged
  html_notebook: default
  pdf_document: default
---
#1. Project Description \ 
(Excerpted from BIOS824-case2-project.pdf) \
Let $R_1, ..., R_n$ denote the reads in a sequencing library and $B_1, ..., B_m$ denote the sequences in a barcode library. Each read and barcode sequence may be assumed to be of the same length say $L$. On a basis of a given ampping algorithm, let $E_{ij}=\{R_i \rightarrow B_j\}$ denote the event that read $i \in \{1, ..., n\}$ has been mapped to barcode $j\in \{1, .., m\}$. \
The objective of this assignment is to write a program that for each barcode provides estimates of the proportion of mapped reads from a FASTQ file. More specifically, the objective of this assignment is to write a program that for each $j=1, ..., m$ calculates $\hat\pi_j = \sum^n_{i=1}[R_i \rightarrow B_j]$.  
  
#2. Algorithm: Probabilistic Error Model 
(Excerpted from bios824-HTS-Day3-handout.pdf)  

## Setup  
(Reference Paper: Li et al.)
- The reference barcode library contains $K$ unique sequences  
- For demonstration symplicity, we assume each barcode is of length L  
- For barcode $k \in \{1, ..., K\}$, the sequence is: $\mathbf{\tilde r}=[\tilde{r} ^k_1, ..., \tilde{r}^k_L]$  
- Each $\tilde{r}^k_j$ represents one of $\{A, T, G, C\}$  
- The sequencing library contains n reads randomly sampled from the barcodes in the reference library  
- $\tilde R$ represents the sequence of the originating barcode and $R$ denotes the sequence of the observed read  

## Probabilistic Error Model  
- The joint probability distribution of the orginating reads conditional on the corresponding observed reads, $P(\tilde{\mathbf{r}}^k |\mathbf{r})$, under the assumption of conditional independence:  
$$
\begin{aligned}
P(\tilde{\mathbf{r}}^k |\mathbf{r})&=P(\tilde{R}=\tilde{\mathbf{r}}^k|R=\mathbf{r}) \\
&=P(\tilde{R}=[\tilde{r}_1, ..., \tilde{r}_L]|R=[r_1, ..., r_L]) \\
&=\prod_{i=1}^L P(\tilde{R}_j=\tilde{r}_j|R=[r_1, ..., r_L]) \\
&=\prod_{i=1}^L P(\tilde{R}_j=\tilde{r}_j|R_j=r_j).
\end{aligned}$$  
- The marginal conditional probability is:  
$$
P(\tilde{R}_j=\tilde{r}_j|R_j=r_j)=
\begin{cases}
1-\epsilon_j, & \tilde{r}_j = r_j \\
\frac{\epsilon_j}{3}, & \tilde{r}_j \neq r_j
\end{cases}
$$  

#3. Installation  
```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```
Make sure to install the package like this:  
```{r, eval=FALSE}
devtools::install_gitlab(repo = "yl477/mapseq",host = "https://gitlab.oit.duke.edu", build_opts = c("--no-resave-data", "--no-manual"))
```

```{r}
library(mapseq)
```

#4. Example
```{r}
data(fasta)
data(fastq)
output_table <- map_ref_read(fasta, fastq, num_mismatch = 2)
output_sorted <- 
head(output_table)
```
### Some Visualization
```{r, fig.height = 10, fig.width = 10}
output_table$Barcode = factor(output_table$Barcode, levels = output_table$Barcode)
ggplot2::ggplot(ggplot2::aes(x=Barcode, y=Proportion), data = output_table) + ggplot2::geom_bar(stat = 'identity') + ggplot2::xlab("Barcode") + ggplot2::ylab("Percentage of Matched Reads(%)")
```

