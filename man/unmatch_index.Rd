% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/Unmatch_base.R
\name{unmatch_index}
\alias{unmatch_index}
\title{this is a function created for determining the unmatched bases between read and ref}
\usage{
unmatch_index(seq1, seq2)
}
\arguments{
\item{seq1}{string type input of sequence1}

\item{seq2}{string type input of sequence2}
}
\value{
numeric output of the index/indices of mismatched base(s)
}
\description{
this is a function created for determining the unmatched bases between read and ref
}
